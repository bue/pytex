####
# title: paper_template.py
#
# author: elmar bucher
# date: 2023-06-00
# license: GPLv3
#
# description:
#   https://doi.org/10.1117/1.JMM.13.4.040101
#   the quality of research (method and execution) is implicit and should shine through the paper.
#   written as you go with the prj.
####

from pytex import pytex
import datetime


#########
# title # 
#########
ls_tex = pytex.documentclass_article(
    s_title = '',
    s_author = 'Elmar Bucher,',
    s_date = '{}'.format(datetime.datetime.today().isoformat(' ')), #'',
    i_fontsize = 11,
    s_geometry = 'letterpaper, textheight=9.8in, textwidth=7.3in',
    es_usepackage = {'graphicx','hyperref','enumitem'},
    b_caption_left = False,
)


################
# Introduction #
################
# what?
# so what?
# clam: scope, novelty, significance (fits into the paper, what is now, significant to read)
# general -> specific; establish a territory (topic], establish a niche (problem) [observation / discovery], occupy the niche (solution) [explanation]
# maybe: outline of the paper in prose; maybe not: good paragraph headings (maybe statements?) will have the same effect.
pytex.section(
    ls_tex = ls_tex,
    s_section = r'Introduction',
    s_comment = 'introduction',
)
pytex.paragraph(
    ls_tex = ls_tex,
    s_paragraph = r'',
    s_comment = '',
)
ls_tex.extend([
r'',
])


#######################
# Material and Method #
#######################
# how was the result generated?
# which engineering was necessary?
# justify the experimental design!
pytex.section(
    ls_tex = ls_tex,
    s_section = r'Method', # Materials and Methods
    s_comment = 'materials_and_methods',
)
pytex.paragraph(
    ls_tex = ls_tex,
    s_paragraph = r'',
    s_comment = '',
)
ls_tex.extend([
r'',
])


#########################
# Results and Discussion #
#########################
# simply the results obtained by the method (evidence),
# figures, tables, text
pytex.section(
    ls_tex = ls_tex,
    s_section = r'Result',
    s_comment = 'results',
)
pytex.paragraph(
    ls_tex = ls_tex,
    s_paragraph = r'',
    s_comment = '',
)
ls_tex.extend([
r'',
])


# discuss the presented results (evidence does not explain itself)
# follow these steps:
# 1. summarize the results
# 2. discuss whether results are expected or unexpected
# 3. compare these results to previous work
# 4. interpreting and explaining the results (often by comparison to a theory or model)
# 5. hypothesizing about their generality
# specific -> general
pytex.section(
    ls_tex = ls_tex,
    s_section = r'Discussion',
    s_comment = 'discussion',
)
pytex.paragraph(
    ls_tex = ls_tex,
    s_paragraph = r'',
    s_comment = '',
)
ls_tex.extend([
r'',
])


##############
# Conclusion #
##############
# conclusion should concisely provide the final and most general key message.
# address all the research questions set out in the introduction (mention and conclude on them).
# give a future perspective.
pytex.section(
    ls_tex = ls_tex,
    s_section = r'Conclusion',
    s_comment = 'conclusion',
)
pytex.paragraph(
    ls_tex = ls_tex,
    s_paragraph = r'',
    s_comment = '',
)
ls_tex.extend([
r'',
])


# Contribution
# Acknowledge
# Founding


##############
# References #
##############
# bibtex!
pytex.bibliography(
    ls_tex=ls_tex,
    s_pathfile_bib ='/home/bue/Documents/books_work/references.bib',
    #s_style_bst='ieeetr',
)


ls_tex.extend([r'\newpage'])


###########
# Figures #
###########
# img
pytex.figure(
    ls_tex,
    ls_figure = [], # path.jpg
    r_height_in = None,
    r_width_in = 2.0,
    s_float = '!htb',
    s_caption = '',
    s_label = '',
    s_comment = None,
)


### end document ###
pytex.end_document(
    ls_tex = ls_tex,
    #ls_software = ['Python3'],
    b_section_poweredby = False,
)


### typeset document ###
s_pathfile='./latex/20230600_paper_template.tex'
pytex.write_tex(s_pathfile=s_pathfile, ls_tex=ls_tex)
pytex.pdflatex(s_pathfile=s_pathfile, b_bibtex=True)

